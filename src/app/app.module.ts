import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewProjectComponent } from './components/projects/new-project/new-project.component';
import { NewProjectModule } from './components/projects/new-project/new-project.module';
import { ProjectComponent } from './components/projects/project/project.component';

@NgModule({
  declarations: [
    AppComponent,
    NewProjectComponent,
    ProjectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NewProjectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
