export interface ProjectsI {
  titlePost: string;
  contentPost: string;
  imagePost?: string;
  id: string;
  tags: string;
  fileRef?: string;
}
