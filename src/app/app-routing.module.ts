import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProjectComponent} from "./components/projects/project/project.component";


const routes: Routes = [{
  path: '', redirectTo: '/home', pathMatch: 'full'
},
  {
    path: 'home',
    loadChildren: () => import('./components/page/home/home.module').then(m => m.HomeModule)
  }, {
    path: 'post:id', component: ProjectComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
