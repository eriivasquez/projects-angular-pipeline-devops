import {Component, OnInit} from '@angular/core';
import list from 'src/app/data/data.json';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  /*public projects: {
    id: string;
    titlePost: string;
    contentPost: string;
    imagePost: string
  }[] = [
    {
      id: '1',
      titlePost: 'Aplicación práctica del Desarrollo Organizacional en las entidades públicas y privadas del Ecuador a través de la metodología aprender haciendo.',
      contentPost: 'El fragmento estándar de Lorem Ipsum utilizado desde el año 1500 se reproduce a continuación para los interesados. Las secciones 1.10.32 y 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero también se reproducen en su forma original exacta, acompañadas de versiones en inglés de la traducción de 1914 de H. Rackham.',
      imagePost: 'https://pickrandom.com//files/images/feature/random-team.jpg'
      //imagePost:'https://ichef.bbci.co.uk/news/ws/410/amz/worldservice/live/assets/images/2016/05/23/160523215725_caminar_1_640x360_istock_nocredit.jpg'
    },
    {
      id: '2',
      titlePost: 'Aplicación práctica del Desarrollo Organizacional en las entidades públicas y privadas del Ecuador a través de la metodología aprender haciendo.',
      contentPost: 'El fragmento estándar de Lorem Ipsum utilizado desde el año 1500 se reproduce a continuación para los interesados. Las secciones 1.10.32 y 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero también se reproducen en su forma original exacta, acompañadas de versiones en inglés de la traducción de 1914 de H. Rackham.',
      imagePost: 'https://pickrandom.com//files/images/feature/random-team.jpg'
      //imagePost:'https://ichef.bbci.co.uk/news/ws/410/amz/worldservice/live/assets/images/2016/05/23/160523215725_caminar_1_640x360_istock_nocredit.jpg'
    },
    {
      id: '3',
      titlePost: 'Aplicación práctica del Desarrollo Organizacional en las entidades públicas y privadas del Ecuador a través de la metodología aprender haciendo.',
      contentPost: 'El fragmento estándar de Lorem Ipsum utilizado desde el año 1500 se reproduce a continuación para los interesados. Las secciones 1.10.32 y 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero también se reproducen en su forma original exacta, acompañadas de versiones en inglés de la traducción de 1914 de H. Rackham.',
      imagePost: 'https://pickrandom.com//files/images/feature/random-team.jpg'
      //imagePost:'https://ichef.bbci.co.uk/news/ws/410/amz/worldservice/live/assets/images/2016/05/23/160523215725_caminar_1_640x360_istock_nocredit.jpg'
    }
  ];*/
  Projects: any = list;

  constructor() {
  }

  ngOnInit(): void {
  }

}
