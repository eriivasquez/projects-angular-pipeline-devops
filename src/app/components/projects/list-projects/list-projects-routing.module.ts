import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListProjectsComponent } from './list-projects.component';

const routes: Routes = [{ path: '', component: ListProjectsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListProjectsRoutingModule { }
